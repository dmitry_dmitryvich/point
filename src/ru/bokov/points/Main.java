package ru.bokov.points;

/**
 * Created by Дмитрий on 23.12.2014.
 */
public class Main {
    public static void main(String[] args) {
        Point a=new Point('A',2,4);
        Point b=new Point('B',2,6);
        System.out.println(a);
        System.out.println(b);
        System.out.println("Расстояние от точки A до B - " + a.howLong(b));
    }
}
